package com.musala.drone.repository;

import com.musala.drone.enums.State;
import com.musala.drone.model.Drone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone,String> {
    @Query("SELECT d FROM Drone d WHERE d.state = 'IDLE'")
    List<Drone> findDroneAvailable();

    Drone findBySerial(String serial);

    List<Drone> findByState(State state);
}
