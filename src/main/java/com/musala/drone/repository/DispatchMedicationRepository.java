package com.musala.drone.repository;

import com.musala.drone.model.DispatchMedication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DispatchMedicationRepository extends JpaRepository<DispatchMedication,Integer> {
}
