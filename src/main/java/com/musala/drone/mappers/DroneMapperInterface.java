package com.musala.drone.mappers;

import com.musala.drone.dto.DroneDto;
import com.musala.drone.model.Drone;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { DispatchMapperInterface.class })
public interface DroneMapperInterface {
    DroneMapperInterface MAPPER = Mappers.getMapper( DroneMapperInterface.class );

    @Mapping(source = "droneSerial", target = "serial")
    @Mapping(source = "modelType", target = "model")
    @Mapping(source = "maxWeight", target = "weightLimit")
    @Mapping(source = "batteryPercentage", target = "batteryCapacity")
    @Mapping(source = "droneState", target = "state")
    public Drone droneDtoToDrone(DroneDto droneDto);

    @InheritInverseConfiguration
    public DroneDto fromDrone(Drone drone);
}
