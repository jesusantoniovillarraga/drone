package com.musala.drone.mappers;

import com.musala.drone.dto.DispatchDto;
import com.musala.drone.model.Dispatch;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { MedicationMapperInterface.class, DroneMapperInterface.class })
public interface DispatchMapperInterface {
    DispatchMapperInterface MAPPER = Mappers.getMapper( DispatchMapperInterface.class );


    @Mapping(source = "creationDate", target = "date")
    @Mapping(source = "droneSerial", target = "drone.serial")
    Dispatch toDispatch(DispatchDto orderItemDto);

    @InheritInverseConfiguration
    DispatchDto fromDispatch(Dispatch orderItemDto);
}
