package com.musala.drone.mappers;

import com.musala.drone.dto.MedicationDto;
import com.musala.drone.model.Medication;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = { DispatchMapperInterface.class })
public interface MedicationMapperInterface {
    MedicationMapperInterface MAPPER = Mappers.getMapper( MedicationMapperInterface.class );

    @Mapping(source = "name", target = "name")
    @Mapping(source = "weight", target = "weight")
    @Mapping(source = "code", target = "code")
    @Mapping(source = "imageUrl", target = "image")
    Medication toMedication(MedicationDto medicationDto);

    @InheritInverseConfiguration
    MedicationDto fromMedication(Medication medication);
}
