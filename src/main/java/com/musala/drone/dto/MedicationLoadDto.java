package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MedicationLoadDto {
    @JsonProperty("code")
    @NotNull
    @Schema(
            description = "Existing medication code",
            type = "string",
            example = "AGP-123"
    )
    private String code;

    @JsonProperty("quantity")
    @Min(1)
    @Max(500)
    @Schema(
            description = "Medication quantity to load",
            type = "integer",
            example = "3"
    )
    private int quantity;
}
