package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class LoadDto {
    @JsonProperty("droneSerial")
    @Size(min = 0, max = 100)
    @NotNull
    @Schema(
            description = "Existing drone serial number",
            type = "string",
            example = "TYG-000"
    )
    private String droneSerial;

    @JsonProperty("creationDate")
    @NotNull
    @Schema(
            description = "Load's creation date",
            type = "string",
            format = "date-time",
            example = "2020-04-28"
    )
    private LocalDate creationDate;

    @JsonProperty("medications")
    @NotNull
    @Size(min=1)
    @Schema(
            description = "Medications to load",
            type = "object array"
    )
    List<@Valid MedicationLoadDto> medications;
}
