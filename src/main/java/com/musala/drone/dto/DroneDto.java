package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.musala.drone.enums.ModelType;
import com.musala.drone.enums.State;
import com.musala.drone.validations.annotation.ValueOfEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DroneDto {

    @JsonProperty("droneSerial")
    @Size(min = 0, max = 100)
    @NotNull
    @NotBlank
    @NotEmpty
    @Schema(
            description = "Drone serial number",
            type = "string",
            example = "TYG-000"
    )
    private String droneSerial;

    @JsonProperty("modelType")
    @NotNull
    @NotEmpty
    @ValueOfEnum(enumClass = ModelType.class)
    @Schema(
            description = "Drone model type",
            type = "string",
            example = "LIGHTWEIGHT"
    )
    private String modelType;

    @JsonProperty("maxWeight")
    @Min(1)
    @Max(500)
    @Schema(
            description = "Max weight than can be carried by the drone",
            type = "integer",
            example = "100"
    )
    private int maxWeight;

    @JsonProperty("batteryPercentage")
    @Min(1)
    @Max(100)
    @Schema(
            description = "Actual percent in the drone's battery",
            type = "integer",
            example = "100"
    )
    private int batteryPercentage;

    @JsonProperty("droneState")
    @NotNull
    @NotEmpty
    @ValueOfEnum(enumClass = State.class)
    @Schema(
            description = "Drone State",
            type = "String",
            example = "IDLE"
    )
    private String droneState;
}
