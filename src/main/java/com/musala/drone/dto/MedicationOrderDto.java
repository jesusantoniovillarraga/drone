package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MedicationOrderDto {

    @JsonProperty("name")
    @Schema(
            description = "Existing medication name",
            type = "string",
            example = "Aqua Cure"
    )
    private String medicationName;

    @JsonProperty("quantity")
    @Schema(
            description = "Medication quantity loaded",
            type = "integer",
            example = "3"
    )
    private int quantity;
}
