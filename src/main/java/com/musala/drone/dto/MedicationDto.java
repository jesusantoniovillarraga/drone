package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class MedicationDto {

    @JsonProperty("name")
    @NotNull
    @Pattern(regexp = "([A-Za-z0-9-_]+)")
    @Schema(
            description = "Medication name",
            type = "string",
            example = "Paracetamol"
    )
    private String name;

    @JsonProperty("weight")
    @NotNull
    @Schema(
            description = "Medication weight",
            type = "integer",
            example = "10"
    )
    private int weight;

    @JsonProperty("code")
    @NotNull
    @Pattern(regexp = "([A-Za-z0-9_]+)")
    @Schema(
            description = "Medication code",
            type = "string",
            example = "AGP-123"
    )
    private String code;

    @JsonProperty("imageUrl")
    @Schema(
            description = "Medication image url",
            type = "string",
            example = "https://imagebank.com/image.png"
    )
    private String imageUrl;
}
