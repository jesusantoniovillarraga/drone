package com.musala.drone.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class DispatchDto {

    @JsonProperty("droneSerial")
    @Size(min = 0, max = 100)
    @NotNull
    @Schema(
            description = "Existing drone serial number",
            type = "string",
            example = "TYG-000"
    )
    private String droneSerial;

    @JsonProperty("creationDate")
    @NotNull
    @Schema(
            description = "Dispatch's creation date",
            type = "string",
            format = "date-time",
            example = "2020-04-28"
    )
    private LocalDate creationDate;
}
