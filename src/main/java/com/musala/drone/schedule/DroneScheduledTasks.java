package com.musala.drone.schedule;

import com.musala.drone.enums.State;
import com.musala.drone.model.Drone;
import com.musala.drone.repository.DroneRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Component
public class DroneScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(DroneScheduledTasks.class);
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

    @Autowired
    private DroneRepository droneRepository;

    @Scheduled(fixedDelay = 1000)
    public void changeDroneStatusToDelivering() {
        List<Drone> loadedDrones = droneRepository.findByState(State.LOADED);
        if(loadedDrones.size() > 0){
            loadedDrones.forEach((drone) ->{
                drone.setState(State.DELIVERING);
                droneRepository.save(drone);
            } );
            log.info("All loaded drones are delivering at {}", dateFormat.format(new Date()));
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void changeDroneStatusToDelivered() {
        List<Drone> deliveringDrones = droneRepository.findByState(State.DELIVERING);
        if(deliveringDrones.size() > 0){
            deliveringDrones.forEach((drone) ->{
                drone.setState(State.DELIVERED);
                droneRepository.save(drone);
            } );
            log.info("All delivering drones has delivered the load at {}", dateFormat.format(new Date()));
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void changeDroneStatusToReturning() {
        List<Drone> deliveredDrones = droneRepository.findByState(State.DELIVERED);
        if(deliveredDrones.size() > 0){
            deliveredDrones.forEach((drone) ->{
                drone.setState(State.RETURNING);
                droneRepository.save(drone);
            } );
            log.info("All delivering drones are returning at {}", dateFormat.format(new Date()));
        }
    }

    @Scheduled(fixedDelay = 1000)
    public void changeDroneStatusToIdle() {
        List<Drone> returningDrones = droneRepository.findByState(State.RETURNING);
        if(returningDrones.size() > 0){
            returningDrones.forEach((drone) ->{
                drone.setState(State.IDLE);
                drone.setBatteryCapacity(drone.getBatteryCapacity() - 25);
                droneRepository.save(drone);
            } );
            log.info("All delivering drones are idle at {}", dateFormat.format(new Date()));
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void checkBatteryLevels() {
        List<Drone> loadedDrones = droneRepository.findAll();
        loadedDrones.forEach((drone) ->{
            log.info("Battery level for drone: {} is {}", drone.getSerial(), drone.getBatteryCapacity());
        } );
        log.info("Drone's battery report finished at {}", dateFormat.format(new Date()));
    }

    @Scheduled(fixedDelay = 10000)
    public void rechargeBatteryLevels() {
        List<Drone> returningDrones = droneRepository.findByState(State.IDLE);
        if(returningDrones.size() > 0){
            returningDrones.forEach((drone) ->{
                if(drone.getBatteryCapacity() < 100){
                    drone.setBatteryCapacity(drone.getBatteryCapacity() + 5);
                    droneRepository.save(drone);
                }
            } );
        }
    }

}
