package com.musala.drone.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Entity
@Getter
@Setter
public class DispatchMedication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToOne
    @JoinColumn(name="dispatch.id", nullable=false)
    private Dispatch dispatch;

    @ManyToOne
    @JoinColumn(name="medication.id", nullable=false)
    private Medication medication;

    @Min(1)
    @NotNull
    private int quantity;

    @NotNull
    private LocalDate creationDate;
}
