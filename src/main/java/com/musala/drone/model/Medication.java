package com.musala.drone.model;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class Medication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Pattern(regexp = "([A-Za-z0-9-_]+)")
    private String name;

    @NotNull
    private int weight;

    @NotNull
    @Pattern(regexp = "([A-Za-z0-9_]+)")
    private String code;

    private String image;

    @OneToMany(mappedBy="medication")
    private List<DispatchMedication> orders;
}
