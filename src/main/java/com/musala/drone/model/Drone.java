package com.musala.drone.model;

import com.musala.drone.enums.ModelType;
import com.musala.drone.enums.State;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Entity
@Getter
@Setter
public class Drone {

    @Id
    @Column(length = 100)
    private String serial;

    @Enumerated(EnumType.STRING)
    @NotNull
    private ModelType model;

    @Max(500)
    @NotNull
    private int weightLimit;

    @Max(100)
    @NotNull
    private int batteryCapacity;

    @Enumerated(EnumType.STRING)
    @NotNull
    private State state;

    @OneToMany(mappedBy="drone")
    private List<Dispatch> dispatches;
}
