package com.musala.drone.controller;

import com.musala.drone.dto.DispatchDto;
import com.musala.drone.dto.MedicationDto;
import com.musala.drone.errors.ApiError;
import com.musala.drone.mappers.DispatchMapperInterface;
import com.musala.drone.repository.DispatchRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/dispatch")
public class DispatchController {

    @Autowired
    private DispatchRepository repository;

    @GetMapping("")
    @Operation(summary = "List all dispatches registered", description = "List all dispatches registered", tags = { "Dispatch" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DispatchDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<List<DispatchDto>> getAllDispatches(){
        List<DispatchDto> result = repository.findAll().stream().map(DispatchMapperInterface.MAPPER::fromDispatch).toList();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
