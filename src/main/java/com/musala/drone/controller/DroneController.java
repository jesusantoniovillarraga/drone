package com.musala.drone.controller;

import com.musala.drone.dto.DroneDto;
import com.musala.drone.dto.LoadDto;
import com.musala.drone.dto.MedicationOrderDto;
import com.musala.drone.enums.State;
import com.musala.drone.errors.ApiError;
import com.musala.drone.mappers.DroneMapperInterface;
import com.musala.drone.model.Dispatch;
import com.musala.drone.model.DispatchMedication;
import com.musala.drone.model.Drone;
import com.musala.drone.model.Medication;
import com.musala.drone.repository.DispatchMedicationRepository;
import com.musala.drone.repository.DispatchRepository;
import com.musala.drone.repository.DroneRepository;
import com.musala.drone.repository.MedicationRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@RestController
@RequestMapping("api/drone")
public class DroneController {

    @Autowired
    private DroneRepository droneRepository;

    @Autowired
    private MedicationRepository medicationRepository;

    @Autowired
    private DispatchRepository dispatchRepository;

    @Autowired
    private DispatchMedicationRepository dispatchMedicationRepository;

    @GetMapping("")
    @Operation(summary = "List all drones registered", description = "List all drones registered", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DroneDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<List<DroneDto>> getAllDrones(){
        List<DroneDto> result = droneRepository.findAll().stream().map(DroneMapperInterface.MAPPER::fromDrone).toList();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @PostMapping("register")
    @Operation(summary = "Register a new drone", description = "Register a new drone", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Drone registered successfully", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<String> regsiterDrone(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Information object to register new drone", required = true)
                                                    @Valid @RequestBody DroneDto drone){
        Drone newDrone = droneRepository.save(DroneMapperInterface.MAPPER.droneDtoToDrone(drone));
        return new ResponseEntity<>("Drone registered successfully", HttpStatus.CREATED);

    }

    @PostMapping("load")
    @Operation(summary = "Load a existing drone with medication", description = "Load a existing drone with medication", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Load successfull", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "404", description = "Drone not found", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "422", description = "Drone is not loaded", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity loadDrone(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(description = "Medication and quantity to load in a existing drone", required = true)
                                        @Valid @RequestBody LoadDto load){

        Drone drone = droneRepository.findBySerial(load.getDroneSerial());

        if(Objects.isNull(drone)){
            return new ResponseEntity<>(
                    new ApiError(HttpStatus.NOT_FOUND,
                            "Drone not found",
                            new Exception("Drone not found")),
                    HttpStatus.NOT_FOUND);
        }
        if(!drone.getState().equals(State.IDLE)){
            return new ResponseEntity<>(
                    new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,
                            "Drone can't be loaded, it's status is: " + drone.getState().name(),
                                new Exception("Drone is not loaded")
                    ), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        if(drone.getBatteryCapacity() <= 25){
            return new ResponseEntity<>(
                    new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,
                            "Drone can't be loaded, it's battery is below 25% ",
                            new Exception("Drone is not loaded")
                    ), HttpStatus.UNPROCESSABLE_ENTITY);
        }

        drone.setState(State.LOADING);
        droneRepository.save(drone);

        Dispatch dispatch = new Dispatch();
        dispatch.setDrone(drone);
        dispatch.setDate(load.getCreationDate());
        dispatch.setOrders(new ArrayList<>());
        dispatchRepository.save(dispatch);
        AtomicInteger totalWeight = new AtomicInteger();
        List<Medication> medications  = new LinkedList<>();
        load.getMedications().forEach((medicationLoad)->{
            Medication medication = medicationRepository.findByCode(medicationLoad.getCode());
            medications.add(medication);
            totalWeight.addAndGet(medication.getWeight() * medicationLoad.getQuantity());
        });
        if(totalWeight.intValue()> drone.getWeightLimit()){
            return new ResponseEntity<>(
                    new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,
                            "Drone can't be loaded, The medication weight if more than the drone's max weight",
                            new Exception("Drone is not loaded")
                    ), HttpStatus.UNPROCESSABLE_ENTITY);
        }
        for(int i = 0; i < medications.size(); i++){
            Medication medication = medications.get(i);
            DispatchMedication order = new DispatchMedication();
            order.setMedication(medication);
            order.setDispatch(dispatch);
            order.setQuantity(load.getMedications().get(i).getQuantity());
            order.setCreationDate(LocalDate.now());
            dispatchMedicationRepository.save(order);
            dispatch.getOrders().add(order);
            medication.getOrders().add(order);
            dispatchRepository.save(dispatch);
            medicationRepository.save(medication);
        }
        drone.getDispatches().add(dispatch);
        drone.setState(State.LOADED);
        droneRepository.save(drone);

        return new ResponseEntity<>("Load successfull", HttpStatus.OK);

    }

    @GetMapping("medicationCheck")
    @Operation(summary = "Check the actual load of existing drone", description = "Check the actual load of existing drone", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MedicationOrderDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "404", description = "Drone not found", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "422", description = "Drone is not loaded", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "406", description = "the drone has not had any dispatch", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity checkLoadedMedication(@Parameter(description = "Serial of existing drone", required = true)
                                                    @RequestParam("serial") String serial){
        Drone drone = droneRepository.findBySerial(serial);
        if(Objects.isNull(drone)){
            return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND,"Drone not found",
                    new Exception("Drone not found")),
                    HttpStatus.NOT_FOUND);
        }
        if(!drone.getState().equals(State.LOADED)){
            return new ResponseEntity<>(new ApiError(HttpStatus.UNPROCESSABLE_ENTITY,
                    "Drone is not loaded",
                    new Exception("Drone is not loaded")),
                    HttpStatus.UNPROCESSABLE_ENTITY);
        }

        List<Dispatch> dispatches = drone
                .getDispatches();

        if(dispatches.size() < 1){
            return new ResponseEntity<>(new ApiError(HttpStatus.NOT_ACCEPTABLE,
                    "the drone has not had any dispatch",
                        new Exception("the drone has not had any dispatch")
            ), HttpStatus.NOT_ACCEPTABLE);
        }

        List<MedicationOrderDto> medications = new ArrayList<>();
        dispatches.get(dispatches.size()-1).getOrders().forEach((x)->{
           MedicationOrderDto medicationOrderDto = new MedicationOrderDto();
           medicationOrderDto.setMedicationName(x.getMedication().getName());
           medicationOrderDto.setQuantity(x.getQuantity());
           medications.add(medicationOrderDto);
        });

        return new ResponseEntity<>(medications, HttpStatus.OK);
    }

    @GetMapping("available")
    @Operation(summary = "List all drones available", description = "List all drones available", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DroneDto.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity<List<DroneDto>> availableDrones(){
        List<DroneDto> available = droneRepository.findDroneAvailable().stream()
                .map(DroneMapperInterface.MAPPER::fromDrone).toList();
        return new ResponseEntity<>(available, HttpStatus.OK);
    }

    @GetMapping("batteryCheck")
    @Operation(summary = "Return the battery level of existing drone", description = "Return the battery level of existing drone", tags = { "Drone" })
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "The battery percent level", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = String.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad Request", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) }),
            @ApiResponse(responseCode = "404", description = "Drone not found", content = {
                    @Content(mediaType = "application/json",
                            schema = @Schema(implementation = ApiError.class)) })
    })
    public ResponseEntity checkDroneBattery(@Parameter(description = "Serial of existing drone", required = true)
                                                @RequestParam("serial") String serial){
        DroneDto drone = DroneMapperInterface.MAPPER.fromDrone(droneRepository.findBySerial(serial));
        if(Objects.isNull(drone)){
            return new ResponseEntity<>(new ApiError(HttpStatus.NOT_FOUND,"Drone not found", new Exception("Drone not found")), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("The battery percent is: " + drone.getBatteryPercentage() + "%", HttpStatus.OK);
    }
}
