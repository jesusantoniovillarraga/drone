package com.musala.drone.controller;

import com.musala.drone.utils.JSONUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class MedicationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void shouldReturnMedicationList() throws Exception {
        this.mockMvc.perform(get("/api/medication")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(JSONUtils.MEDICATIONLIST));
    }
}
