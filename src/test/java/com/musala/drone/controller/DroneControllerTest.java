package com.musala.drone.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.musala.drone.dto.DroneDto;
import com.musala.drone.utils.JSONUtils;
import jdk.jfr.ContentType;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class DroneControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    @Order(1)
    public void shouldReturnDroneList() throws Exception {
        this.mockMvc.perform(get("/api/drone")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(JSONUtils.DRONELIST));
    }

    @Test
    @Order(2)
    public void shouldReturnDroneAvailableList() throws Exception {
        this.mockMvc.perform(get("/api/drone/available")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().json(JSONUtils.DRONELIST));
    }

    @Test
    @Order(3)
    public void shouldReturnDroneBatteryPercent() throws Exception {
        this.mockMvc.perform(get("/api/drone/batteryCheck").param("serial","ABC-123")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("The battery percent is: 100%")));
    }

    @Test
    @Order(4)
    public void shouldReturnOkRegisterDrone() throws Exception {
        DroneDto drone = new DroneDto();
        drone.setDroneSerial("ABC-999");
        drone.setModelType("LIGHTWEIGHT");
        drone.setMaxWeight(100);
        drone.setBatteryPercentage(100);
        drone.setDroneState("IDLE");
        ObjectMapper objectMapper = new ObjectMapper();
        this.mockMvc.perform(post("/api/drone/register").content(objectMapper.writeValueAsString(drone)).contentType("application/json")).andDo(print()).andExpect(status().isCreated())
                .andExpect(content().string(containsString("Drone registered successfully")));
    }

    @Test
    @Order(5)
    public void shouldReturnOkLoadDrone() throws Exception {
        this.mockMvc.perform(post("/api/drone/load").content(JSONUtils.LOAD).contentType("application/json")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Load successfull")));
    }

    @Test
    @Order(6)
    public void shouldReturnDroneMedicationLoad() throws Exception {
        this.mockMvc.perform(get("/api/drone/medicationCheck").param("serial","ABC-123")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString(JSONUtils.MEDICATIONLOAD)));
    }
}
